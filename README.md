# Garmentory #

Technical Assessment project for Lululemon.

### How do I get set up? ###

* Navigate to the project directory containing *Podfile* and execute `pod install`.
* Open *Garmentory.xcworkspace*
* Run *Garmentory*

### About Garmentory ###

* Written in Swift.
* User interface built with UIKit.
* Generic UITableView subclass, `RealmDataSource` that operates on any RealmSwift.Object that conforms to `Displayable`. Supports sorting.
* `SortSegmentedControl` which populates its items based on any model class that conforms to `Sortable`. 
* DatabaseService abstracts CRUD operations (only supports `create` based on test project spec.)
* Constants struct for accessing app-wide constants (fonts, colors, dimensions, strings, etc.)
* UserPreferences struct for reading and writing user preferences (sort type) to UserDefaults.
* Unit tests for testing DatabaseService's basic functionality.
* UI tests for testing creation view controller's save functionality and existence of UI elements outlined in project specification.

### Improvements ###
* Support missing CRUD operations.
* If the app needed to support listing and creating multiple models, I would opt for making the list and creation view controllers generic so that they can be reused.
* Prevent creation of duplicate Garments (garments with identical names.)


### Dependencies ###

* RealmSwift