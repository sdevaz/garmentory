//
//  GarmentoryTests.swift
//  GarmentoryTests
//
//  Created by Sage DeVaz on 2021-09-10.
//

import XCTest
import RealmSwift
@testable import Garmentory

class GarmentoryTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Use an in-memory Realm identified by the name of the current test.
        // This ensures that each test can't accidentally access or modify the data
        // from other tests or the application itself, and because they're in-memory,
        // there's nothing that needs to be cleaned up.
        Realm.Configuration.defaultConfiguration.inMemoryIdentifier = self.name
    }

    func test_databaseGarments_shouldBeEmpty() {
        if let realm = try? Realm() {
            let dbService = DatabaseService(realm: realm)
            XCTAssertEqual(dbService.garmentsResults.count, 0, "garment count should be 0")
        } else {
            XCTFail("failed to establish realm connection")
        }
    }

    func test_GarmentCount_AfterCreatingGarment_shouldBeOne() {
        if let realm = try? Realm() {
            let dbService = DatabaseService(realm: realm)
            _ = dbService.save(garment: Garment(name: "Test Garment"))
            XCTAssertEqual(realm.objects(Garment.self).count, 1, "garment count after save should be 1")
        } else {
            XCTFail("failed to establish realm connection")
        }
    }

    func test_CreateGarment() {
        if let realm = try? Realm() {
            let dbService = DatabaseService(realm: realm)
            let testDate = Date(timeIntervalSince1970: 10000)
            let testName = "Blue Pants"
            let testID = "GARMENT_ID"
            guard let _ = dbService.save(garment: Garment(id: testID,
                                                          name: testName,
                                                          dateCreated: testDate))
            else {
                XCTFail("failed to save garment, save garment method returned nil")
                return
            }

            guard let garment = realm.objects(Garment.self).first else {
                XCTFail("failed to save garment, no saved garments found in database")
                return
            }

            XCTAssertEqual(garment.name,
                           testName,
                           "created garment should have given test name, \(testName)")
            XCTAssertEqual(garment.id,
                           testID,
                           "created garment should have given test id, \(testID)")
            XCTAssertEqual(garment.dateCreated,
                           testDate,
                           "created garment should have given test date, \(testDate.description)")
        } else {
            XCTFail("failed to establish realm connection")
        }
    }
}
