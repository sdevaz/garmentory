//
//  Displayable.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-12.
//

import Foundation

/**
 Displayable Protocol used for providing a name. Used in RealmDataSource.
 */
protocol Displayable {
    var displayName: String { get }
}
