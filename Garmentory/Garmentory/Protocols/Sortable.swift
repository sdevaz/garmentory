//
//  Sortable.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-14.
//

import Foundation

/**
 Sortable Protocol used for providing available property names to sort Realm Objects by
 */
protocol Sortable {
    static var sortTypes: [SortType] { get }
}
