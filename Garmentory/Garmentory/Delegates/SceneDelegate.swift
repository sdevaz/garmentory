//
//  SceneDelegate.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-10.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        let garmentListVC = GarmentListViewController()
        let navigationVC = UINavigationController(rootViewController: garmentListVC)
        window.rootViewController = navigationVC
        self.window = window
        window.makeKeyAndVisible()
    }
}
