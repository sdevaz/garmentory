//
//  SortSegmentedControl.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-13.
//

import Foundation
import UIKit


/**
 A segmented control for providing sort type options
 */
class SortSegmentedControl: UISegmentedControl {
    public var sortTypeChangedHandler: ((SortType) -> Void)? {
        didSet {
            addTarget(self, action: #selector(valueChanged(_:)), for: .valueChanged)
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    required init(sortable: Sortable.Type) {
        super.init(items: sortable.sortTypes.map { $0.title })
    }

    @objc private func valueChanged(_ sender: UISegmentedControl) {
        if let sortTypeChangedHandler = sortTypeChangedHandler,
           let selectedSort = SortType.init(rawValue: sender.selectedSegmentIndex) {
            sortTypeChangedHandler(selectedSort)
        }
    }

    public func setSortType(_ sortType: SortType) {
        selectedSegmentIndex = sortType.index
    }
}
