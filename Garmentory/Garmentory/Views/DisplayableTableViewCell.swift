//
//  GarmentTableViewCell.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-10.
//

import Foundation
import UIKit

/**
 Basic table view cell that displays anything that conforms to Displayable
 */
class DisplayableTableViewCell: UITableViewCell {

    public static let reuseIdentifier = NSStringFromClass(DisplayableTableViewCell.self)

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        textLabel?.font = Constants.Style.Font.bodyBold
    }

    required init(coder: NSCoder) {
        fatalError("NSCoding not supported")
    }

    public func configure(displayable: Displayable) {
        textLabel?.text = displayable.displayName
    }
}
