//
//  AlertHelper.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-14.
//

import Foundation
import UIKit

/**
 A helpful interface for quickly presenting alert controllers
 */
struct AlertHelper {

    static func showErrorAlert(presenter: UIViewController,
                               title: String?,
                               message: String?,
                               completion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: Constants.Strings.General.OK, style: .default, handler: nil))
        presenter.present(alert, animated: true, completion: completion)
    }
}
