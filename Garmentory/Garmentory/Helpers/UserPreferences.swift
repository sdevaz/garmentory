//
//  UserPreferences.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-10.
//

import Foundation

/**
 A helpful interface for reading/writing user preferences to UserDefaults
 */
struct UserPreferences {

    // The default sort type
    // Used in GarmentListViewController to initialize data
    static var sortType: SortType {
        get {
            if let sortType = UserDefaults.standard.value(forKey: Constants.UserPreferences.Key.sortType) as? Int {
                return SortType(rawValue: sortType) ?? Constants.UserPreferences.Default.sortType
            } else {
                UserPreferences.sortType = Constants.UserPreferences.Default.sortType
                return UserPreferences.sortType
            }
        }

        set {
            UserDefaults.standard.setValue(newValue.index, forKey: Constants.UserPreferences.Key.sortType)
        }
    }
}
