//
//  Constants.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-10.
//

import Foundation
import UIKit

/**
 Helper struct for accessing app wide constants
 - Strings
 - Sort
 - Style
 - User Preferences
 */
struct Constants {

    struct Strings {
        struct Title {
            static let garmentListViewController = NSLocalizedString("Title.GarmentListViewController",
                                                                     comment: "title for GarmentListViewController")
            static let createGarmentViewController = NSLocalizedString("Title.CreateGarmentViewController",
                                                                       comment: "title for CreateGarmentViewController")
        }

        struct General {
            static let add = NSLocalizedString("General.Add",
                                               comment: "general-use add")
            static let save = NSLocalizedString("General.Save",
                                                comment: "general-use save")
            static let cancel = NSLocalizedString("General.Cancel",
                                                comment: "general-use cancel")
            static let OK = NSLocalizedString("General.OK",
                                                comment: "general-use OK")
            static let error = NSLocalizedString("General.Error",
                                                comment: "general-use OK")
        }

        struct Error {
            static let readData = NSLocalizedString("Error.Read.Data",
                                                    comment: "error occurred trying to read data")
            static let writeData = NSLocalizedString("Error.Write.Data",
                                                     comment: "error occurred trying to write data")
            static let garmentNameRequired = NSLocalizedString("Error.Validation.Name.Required",
                                                               comment: "required field garment name is missing")
        }

        struct Garment {
            static let garmentName = NSLocalizedString("Garment.Name",
                                                       comment: "garment name title for input textfield")
        }
    }

    struct Style {
        struct Color {
                static let text = UIColor.label
                static let background = UIColor.systemBackground
                static let accent = UIColor(red: 38/255.0, green: 194/255.0, blue: 129/255.0, alpha: 1.0)
        }

        struct Font {
            static let body = UIFont.systemFont(ofSize: 15, weight: .regular)
            static let bodyBold = UIFont.systemFont(ofSize: 15, weight: .bold)
            static let subtitle = UIFont.systemFont(ofSize: 13, weight: .regular)
            static let header = UIFont.systemFont(ofSize: 18, weight: .bold)
        }

        struct Size {
            static let segmentedControlHeight: CGFloat = 40.0
            static let textFieldHeight: CGFloat = 40.0
        }

        struct Spacing {
            static let vertical: CGFloat = 10.0
        }

        struct Format {
            static let date = "MM-dd-yyyy HH:mm:ss"
        }
    }

    struct UserPreferences {
        struct Key {
            static let sortType = "UserPreferences.Keys.SortType"
        }

        struct Default {
            static let sortType = SortType.alphabetical
        }
    }
}
