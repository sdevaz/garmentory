//
//  RealmDataSource.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-11.
//

import Foundation
import UIKit
import RealmSwift

/**
 A convenient UITableViewDataSource for Realm Objects
 */
class RealmDataSource: NSObject, UITableViewDataSource {

    private var model: Object.Type
    private var notificationToken: NotificationToken?
    private var initialize: (() -> Void)?
    private var update: (([IndexPath], [IndexPath], [IndexPath]) -> Void)?
    private var error: ((Error) -> Void)?
    private var sort: String?
    private var results: Results<Object>?

    /**
     - Parameters:
         - model: The model's type (must be a Realm.Object)
         - sort: The name of the model's property to sort by
         - initialize: A closure to be executed when data is initially loaded
         - update: A closure to be executed when the data is updated. The closure
                    is passed index paths that were involved in deletions, insertions, modifications
         - error: A closure to be executed when there is an error
     */
    init(model: Object.Type,
         sort: String,
         initialize: (() -> Void)? = nil,
         update: (([IndexPath], [IndexPath], [IndexPath]) -> Void)? = nil,
         error: ((Error) -> Void)? = nil) {
        self.model = model
        self.initialize = initialize
        self.update = update
        self.error = error
        self.results = DatabaseService.shared.realm.objects(model).sorted(byKeyPath: sort)
        super.init()
        setupResultsObserver()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let object = results?[indexPath.row],
           let displayable = object as? Displayable,
           let cell = tableView.dequeueReusableCell(withIdentifier: DisplayableTableViewCell.reuseIdentifier,
                                                    for: indexPath) as? DisplayableTableViewCell {
            cell.configure(displayable: displayable)
            return cell
        }

        return UITableViewCell()
    }

    func setSortType(_ sort: String) {
        // reconfigure results observer
        self.sort = sort
        self.results = DatabaseService.shared.realm.objects(model).sorted(byKeyPath: sort)
        setupResultsObserver()
    }

    private func setupResultsObserver() {
        guard let results = results else { return }
        self.notificationToken = results.observe { (changes: RealmCollectionChange) in
            switch changes {
            case .initial:
                if let initialize = self.initialize {
                    initialize()
                }
            case .update(_, let deletions, let insertions, let modifications):
                if let update = self.update {
                    update(deletions.map { IndexPath(row: $0, section: 0) },
                           insertions.map { IndexPath(row: $0, section: 0) },
                           modifications.map { IndexPath(row: $0, section: 0) })
                }
            case .error(let err):
                if let error = self.error {
                    error(err)
                }
            }
        }
    }
}
