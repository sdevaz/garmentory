//
//  DatabaseService.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-10.
//

import Foundation
import RealmSwift

/**
 Service for reading and writing objects to a persisted database.
 */
final class DatabaseService {
    static let shared = DatabaseService()
    private (set) var realm: Realm
    public var garmentsResults: Results<Garment>

    init(realm: Realm? = nil) {
        if let realm = realm {
            self.realm = realm
        } else {
            do {
                self.realm = try Realm()
            } catch let error {
                // TODO: Log error to backend, this should not happen in production.
                // Make sure to handle migrations.
                // deliberately forcing crash for development.
                fatalError("Failed to open Realm. Error: \(error.localizedDescription)")
            }
        }
        garmentsResults = self.realm.objects(Garment.self)
    }

    public func save(garment: Garment) -> Garment? {
        var result: Garment?
        do {
            try realm.write {
                realm.add(garment)
                result = garment
            }
        } catch let error {
            // Handle error
            print(error.localizedDescription)
            result = nil
        }
        return result
    }
}
