//
//  SortType.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-14.
//

import Foundation

/**
 SortType provides an convenient interface to handle sort types
 */
enum SortType: Int, CaseIterable {
    case alphabetical,
         dateCreated

    var title: String {
        switch self {
        case .alphabetical:
            return NSLocalizedString("SortType.Alphabetical",
                                     comment: "display name for alphabetical sort type")
        case .dateCreated:
            return NSLocalizedString("SortType.DateCreated",
                                     comment: "display name for date created sort type")
        }
    }

    var name: String {
        switch self {
        case .alphabetical:
            return "name"
        case .dateCreated:
            return "dateCreated"
        }
    }

    var index: Int {
        self.rawValue
    }
}
