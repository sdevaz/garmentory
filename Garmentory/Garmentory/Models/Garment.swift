//
//  GarmentObject.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-10.
//

import RealmSwift

/**
 Garment Model class. Subclass of RealmSwiftObject.
 */
class Garment: Object {
    @Persisted var id = UUID().uuidString
    @Persisted var name = ""
    @Persisted var dateCreated = Date()

    convenience init(id: String = UUID().uuidString, name: String, dateCreated: Date = Date()) {
        self.init()
        self.name = name
        self.id = id
        self.dateCreated = dateCreated
    }
}

extension Garment: Displayable {
    var displayName: String {
        name
    }
}

extension Garment: Sortable {
    static var sortTypes: [SortType] {
        return SortType.allCases
    }
}
