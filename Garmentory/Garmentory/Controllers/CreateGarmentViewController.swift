//
//  CreateGarmentViewController.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-10.
//

import UIKit

/**
 Delegate protocol to handle creation of garments
 */
protocol CreateGarmentViewControllerDelegate: AnyObject {
    func didFinish()
}

/**
 UI for creating Garments.
 */
final class CreateGarmentViewController: UIViewController {

    private let inputTitleLabel = UILabel()
    private let inputTextField = UITextField()
    public weak var delegate: CreateGarmentViewControllerDelegate?
    private lazy var addButton = UIBarButtonItem(title: Constants.Strings.General.save,
                                    style: .plain,
                                    target: self,
                                    action: #selector(saveGarment))

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        inputTextField.becomeFirstResponder()
    }

    private func setupUI() {
        title = Constants.Strings.Title.createGarmentViewController
        view.backgroundColor = Constants.Style.Color.background

        addButton.isEnabled = !(inputTextField.text?.isEmpty ?? true)
        addButton.tintColor = Constants.Style.Color.accent

        let cancelButton = UIBarButtonItem(title: Constants.Strings.General.cancel,
                                        style: .plain,
                                        target: self,
                                        action: #selector(cancel))
        cancelButton.tintColor = Constants.Style.Color.text

        navigationItem.rightBarButtonItem = addButton
        navigationItem.leftBarButtonItem = cancelButton

        inputTitleLabel.text = Constants.Strings.Garment.garmentName
        inputTitleLabel.font = Constants.Style.Font.header

        inputTextField.borderStyle = .roundedRect
        inputTextField.delegate = self
        layoutUI()
    }

    private func layoutUI() {
        inputTitleLabel.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(inputTitleLabel)

        inputTextField.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(inputTextField)

        NSLayoutConstraint.activate([
            inputTitleLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor,
                                                 constant: Constants.Style.Spacing.vertical),
            inputTitleLabel.leadingAnchor.constraint(equalTo: view.layoutMarginsGuide.leadingAnchor),
            inputTitleLabel.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            inputTitleLabel.bottomAnchor.constraint(equalTo: inputTextField.topAnchor,
                                                    constant: -1*Constants.Style.Spacing.vertical)
        ])

        NSLayoutConstraint.activate([
            inputTextField.leadingAnchor.constraint(equalTo: inputTitleLabel.leadingAnchor),
            inputTextField.trailingAnchor.constraint(equalTo: view.layoutMarginsGuide.trailingAnchor),
            inputTextField.heightAnchor.constraint(equalToConstant: Constants.Style.Size.textFieldHeight)
        ])
    }

    @objc func saveGarment() {
        guard let inputText = inputTextField.text, !inputText.isEmpty else {
            AlertHelper.showErrorAlert(presenter: self,
                                       title: Constants.Strings.General.error,
                                       message: Constants.Strings.Error.garmentNameRequired)
            return
        }

        guard let _ = DatabaseService().save(garment: Garment(name: inputText)) else {
            AlertHelper.showErrorAlert(presenter: self,
                                       title: Constants.Strings.General.error,
                                       message: Constants.Strings.Error.writeData)
            return
        }

        delegate?.didFinish()
    }

    @objc func cancel() {
        delegate?.didFinish()
    }
}

// MARK: Textfield Delegate
extension CreateGarmentViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        if let initialText = textField.text {
            let text = (initialText as NSString).replacingCharacters(in: range, with: string)
            addButton.isEnabled = !text.isEmpty
        }
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
