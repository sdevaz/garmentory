//
//  GarmentListViewController.swift
//  Garmentory
//
//  Created by Sage DeVaz on 2021-09-10.
//

import UIKit

/**
 Displays saved Garments in a UITableView and provides sorting with a segmented control.
 */
final class GarmentListViewController: UIViewController {

    private let tableView = UITableView()
    private let segmentedControl = SortSegmentedControl(sortable: Garment.self)
    private var dataSource: RealmDataSource?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupDataSource()
        setupUI()
    }

    private func setupDataSource() {
        self.dataSource =
            RealmDataSource(model: Garment.self, sort: UserPreferences.sortType.name,
                            initialize: {self.tableView.reloadData()},
                            update: { deletions, insertions, modifications in
                                self.tableView.beginUpdates()
                                self.tableView.insertRows(at: insertions,
                                                          with: .automatic)
                                self.tableView.deleteRows(at: deletions,
                                                          with: .automatic)
                                self.tableView.reloadRows(at: modifications,
                                                          with: .automatic)
                                self.tableView.endUpdates()},
                            error: { error in
                                print(error.localizedDescription)
                                AlertHelper.showErrorAlert(presenter: self,
                                                           title: Constants.Strings.General.error,
                                                           message: Constants.Strings.Error.readData)})
    }

    private func setupUI() {
        title = Constants.Strings.Title.garmentListViewController
        view.backgroundColor = Constants.Style.Color.background

        let addButton = UIBarButtonItem(barButtonSystemItem: .add,
                                        target: self,
                                        action: #selector(addGarment))
        addButton.tintColor = Constants.Style.Color.accent
        navigationItem.rightBarButtonItem = addButton

        segmentedControl.sortTypeChangedHandler = { sortType in
            UserPreferences.sortType = sortType
            self.dataSource?.setSortType(sortType.name)
        }
        segmentedControl.setSortType(UserPreferences.sortType)
        segmentedControl.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(segmentedControl)

        tableView.dataSource = dataSource
        tableView.register(DisplayableTableViewCell.self,
                           forCellReuseIdentifier: DisplayableTableViewCell.reuseIdentifier)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)

        layoutUI()
    }

    private func layoutUI() {
        NSLayoutConstraint.activate([
            segmentedControl.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor),
            segmentedControl.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            segmentedControl.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            segmentedControl.heightAnchor.constraint(equalToConstant: Constants.Style.Size.segmentedControlHeight),
            segmentedControl.bottomAnchor.constraint(equalTo: tableView.topAnchor)
        ])

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: segmentedControl.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor)
        ])
    }

    @objc func addGarment() {
        let addVC = CreateGarmentViewController()
        addVC.delegate = self
        present(UINavigationController(rootViewController: addVC),
                animated: true,
                completion: nil)
    }
}

// MARK: CreateGarmentViewController Delegate
extension GarmentListViewController: CreateGarmentViewControllerDelegate {
    func didFinish() {
        dismiss(animated: true, completion: nil)
    }
}
