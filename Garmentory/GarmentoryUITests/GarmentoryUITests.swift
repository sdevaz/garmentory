//
//  GarmentoryUITests.swift
//  GarmentoryUITests
//
//  Created by Sage DeVaz on 2021-09-10.
//

import XCTest
@testable import Garmentory

class GarmentoryUITests: XCTestCase {

    let app = XCUIApplication()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        app.launch()
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state -
        // such as interface orientation - required for your tests before they run.
        // The setUp method is a good place to do this.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            // This measures how long it takes to launch your application.
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }

    func test_saveGarmentButton_shouldBeDisabledInitially() throws {
        let listNavigationBar = app.navigationBars["List"]
        listNavigationBar.buttons["Add"].tap()
        let addNavigationBar = app.navigationBars["ADD"]
        let saveButton = addNavigationBar.buttons["Save"]
        XCTAssertEqual(saveButton.isEnabled, false)
    }

    func test_saveGarmentButton_AfterClearingTextfield_shouldBeDisabled() throws {
        let listNavigationBar = app.navigationBars["List"]
        listNavigationBar.buttons["Add"].tap()
        let addNavigationBar = app.navigationBars["ADD"]
        app.keys["T"].tap()
        app.keys["delete"].tap()
        let saveButton = addNavigationBar.buttons["Save"]
        XCTAssertEqual(saveButton.isEnabled, false)

    }

    func test_saveGarmentButton_withPopulatedTextfield_shouldBeEnabled() throws {
        let listNavigationBar = app.navigationBars["List"]
        listNavigationBar.buttons["Add"].tap()
        let addNavigationBar = app.navigationBars["ADD"]
        app.keys["T"].tap()
        let saveButton = addNavigationBar.buttons["Save"]
        XCTAssertEqual(saveButton.isEnabled, true)
    }

    func test_GarmentListViewController_shouldHaveRequiredElements() throws {
        let listNavigationBar = app.navigationBars["List"]
        XCTAssert(listNavigationBar.exists, "navigation bar should exist")

        let addNavBarButton = listNavigationBar.buttons["Add"]
        XCTAssert(addNavBarButton.exists, "add navbar button should exist")

        let alphaSegmentedControlButton = app.segmentedControls.buttons["Alpha"]
        XCTAssert(alphaSegmentedControlButton.exists, "segmented control alpha button should exist")

        let creationTimeSegmentedControlButton = app.segmentedControls.buttons["Creation Time"]
        XCTAssert(creationTimeSegmentedControlButton.exists, "segmented control creation time button should exist")

        let tableView = app.tables.element
        XCTAssert(tableView.exists, "tableView should exist")
    }

    func test_CreateGarmentViewController_shouldHaveRequiredElements() throws {
        let listNavigationBar = app.navigationBars["List"]
        listNavigationBar.buttons["Add"].tap()
        let addNavigationBar = app.navigationBars["ADD"]
        XCTAssert(addNavigationBar.exists, "navigation bar should exist")

        let saveNavBarButton = addNavigationBar.buttons["Save"]
        XCTAssert(saveNavBarButton.exists, "save navbar button should exist")

        let cancelNavBarButton = addNavigationBar.buttons["Cancel"]
        XCTAssert(cancelNavBarButton.exists, "cancel navbar button should exist")

        let textField = app.textFields.element
        XCTAssert(textField.exists, "textField should exist")
    }
}
